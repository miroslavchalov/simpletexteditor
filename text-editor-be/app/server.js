const express = require('express');
const http = require('http');
const app = express();
const upload = require('express-fileupload');
const fs = require('fs');
const bodyParser = require('body-parser');
const textsPath = __dirname + '/upload';

app.use(upload());
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

/**
 * Application starts on port 3000
 */
app.listen(3000, () => {
    console.log('Express web app on localhost:3000')
});

/**
 * POST /upload
 * Expects to have a request body with fileName and textContent properties.
 * It saves the specified file name with the text content in /upload directory
 *
 * Returns 201
 * Returns 400 - when error occurs
 */
app.post('/upload', (req, res) => {
    fs.writeFileSync(`${textsPath}/${req.body.fileName}`, req.body.textContent, (err) => {
        console.error(err.message);
        res.status(400).json({error: 'something went wrong'});
    });
    res.status(201);
    res.send();
});

/**
 * GET /texts
 * Returns a JSON array containing all file names located at /upload directory.
 *
 * Returns 200
 * Returns 400 - when error occurs
 */
app.get('/texts', (req, res) => {
    fs.readdir(textsPath, (err, files) => {
        if (err) {
            console.error(err.message);
            res.status(400).json({error: 'something went wrong'});
        }

        res.header('Content-Type', 'application/json');
        res.status(200);
        res.send(files);
    })
});

/**
 * GET /texts/:file
 * Returns a JSON containing the contents of a specified file from /uploads directory.
 *
 * Returns 200
 * Returns 404 - when there is no such file
 * Returns 400 - if other error occurs
 */
app.get('/texts/:file', (req, res) => {
    let fileName = req.params.file;
    fs.readFile(`${textsPath}/${fileName}`, 'utf8', (err, data) => {
        if (err && err.code === 'ENOENT') {
            console.error(err.message);
            res.status(404).json({error: 'File not found'});
        } else if (err) {
            console.error(err.message);
            res.status(400).json({error: 'something went wrong'});
        }
        res.header('Content-Type', 'application/json');
        res.status(200);
        res.send({document: data});
    })
});

/**
 * DELETE /texts/:file
 * Returns 200 - success message for deleted file name
 * Returns 404 - when there is no such file
 * Returns 400 - if other error occurs
 */

app.delete('/texts/:file', (req, res) => {
    let fileName = req.params.file;
    fs.unlink(`${textsPath}/${fileName}`,(err) => {
        if (err && err.code === 'ENOENT') {
            console.error(err.message);
            res.status(404).json({error: 'File not found'});
        }
    });
    res.header('Content-Type', 'application/json');
    res.status(200);
    res.send(`${fileName} is deleted`);
});

